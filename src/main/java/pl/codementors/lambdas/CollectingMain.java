package pl.codementors.lambdas;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Example for collecting items to new collections.
 *
 * @author psysiu
 */
public class CollectingMain {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(2, 4, 3, 0, 9, 8) ;

        List filtered = list.stream().filter(integer -> integer < 4).collect(Collectors.toList());
        filtered.forEach(item -> System.out.println(item));

        System.out.println();

        list.stream().filter(item -> item > 3 && item < 8).forEach(item -> System.out.println(item));
    }

}
