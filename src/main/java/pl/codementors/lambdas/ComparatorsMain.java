package pl.codementors.lambdas;

import java.util.*;

/**
 * Example for comparing objects with lambdas.
 *
 * @author psysiu
 */
public class ComparatorsMain {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(2, 4, 3, 0, 9, 8) ;

        //Anonymous class.
        Collections.sort(list, new Comparator<Integer>() {
            @Override
            public int compare(Integer i1, Integer i2) {
                return i1 - i2;
            }
        });

        list.forEach(item -> System.out.println(item));
        System.out.println();

        //Lambda expression. More than one method argument requires ().
        Collections.sort(list, (i1, i2) -> {
            return i2 - i1;
        });

        list.forEach(item -> System.out.println(item));
        System.out.println();


        //Short lambda.
        Collections.sort(list, (i1, i2) -> i1 - i2);

        list.forEach(item -> System.out.println(item));
        System.out.println();

    }

}
