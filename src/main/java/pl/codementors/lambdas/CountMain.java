package pl.codementors.lambdas;

import java.util.Arrays;
import java.util.List;

/**
 * Example with counting elements.
 *
 * @author psysiu
 */
public class CountMain {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(2, 4, 3, 0, 9, 8) ;

        System.out.println(list.stream().count());
        System.out.println(list.stream().filter(item -> item > 4).count());
    }

}
