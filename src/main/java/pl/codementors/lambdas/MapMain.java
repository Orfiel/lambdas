package pl.codementors.lambdas;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Example for changing or mapping elements to different types.
 *
 * @author psysiu
 */
public class MapMain {

    public static void main(String[] args) {
        List<Integer> intList = Arrays.asList(2, 4, 3, 0, 9, 8) ;
        List<String> stringList = Arrays.asList("wlazł", "kotek", "na") ;

        intList.stream().map(new Function<Integer, Integer>() {
            @Override
            public Integer apply(Integer value) {
                return 2 * value;
            }
        }).forEach(new Consumer<Integer>() {
            @Override
            public void accept(Integer value) {
                System.out.println(value);
            }
        });

        System.out.println();

        intList.stream().map(integer -> 2 * integer).forEach(integer -> System.out.println(integer));

        System.out.println();

        stringList.stream().map(new Function<String, String>() {
            @Override
            public String apply(String value) {
                return value.toUpperCase();
            }
        }).forEach(new Consumer<String>() {
            @Override
            public void accept(String value) {
                System.out.println(value);
            }
        });

        System.out.println();

        stringList.stream().map(value -> value.toUpperCase()).forEach(item -> System.out.println(item));

        System.out.println();

        //Instead of declaring item and calling method on it method reference can be used.
        stringList.stream().map(String::toUpperCase).forEach(System.out::println);

        System.out.println();

        System.out.println(stringList.stream().mapToInt(String::length).average().getAsDouble());
        System.out.println(stringList.stream().mapToInt(String::length).max().getAsInt());
        System.out.println(stringList.stream().mapToInt(String::length).min().getAsInt());
        System.out.println(stringList.stream().mapToInt(String::length).sum());
    }

}
