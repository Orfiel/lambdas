package pl.codementors.lambdas;

import java.util.Arrays;
import java.util.List;

/**
 * Some collection manipulation tasks.
 *
 * @author psysiu
 */
public class Tasks {

    public static void main(String[] args) {

        List<String> list = Arrays.asList("wolf", "tiger", "turtle", "tortoise", "puma", "wolverine", "");

        System.out.println("task1:");
        task1(list);
        System.out.println("\ntask2:");
        task2(list);
        System.out.println("\ntask3:");
        task3(list);
        System.out.println("\ntask4:");
        task4(list);
        System.out.println("\ntask5:");
        task5(list);
        System.out.println("\ntask6:");
        task6(list);
        System.out.println("\ntask7:");
        task7(list);
        System.out.println("\ntask8:");
        task8(list);
        System.out.println("\ntask9:");
        task9(list);
        System.out.println("\ntask10:");
        task10(list);
        System.out.println("\ntask11:");
        task11(list);
        System.out.println("\ntask12:");
        task12(list);

    }

    public static void task1(List<String> list) {
        //print all elements

    }

    public static void task2(List<String> list) {
        //print all elements in upper case
    }

    public static void task3(List<String> list) {
        //print only elements with 4 letters
    }

    public static void task4(List<String> list) {
        //print all elements sorted by number of letters
    }

    public static void task5(List<String> list) {
        //print all elements starting with 't'
    }

    public static void task6(List<String> list) {
        //print number of all elements starting with w
    }

    public static void task7(List<String> list) {
        //print "true" if any of the elements is empty, "false" otherwise
    }

    public static void task8(List<String> list) {
        //print "true" if all elements are empty, "false otherwise
    }

    public static void task9(List<String> list) {
        //print number of elements with more than 4 letters

    }

    public static void task10(List<String> list) {
        //print highest number of letters
    }

    public static void task11(List<String> list) {
        //print lowest number of letters
    }

    public static void task12(List<String> list) {
        //print sum of all numbers of letters
    }
}
