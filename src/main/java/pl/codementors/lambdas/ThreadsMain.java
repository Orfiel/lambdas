package pl.codementors.lambdas;

/**
 * How lambdas can be used to create threads.
 *
 * @author psysiu
 */
public class ThreadsMain {

    public static void main(String[] args) {

        //Using anonymous class.
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("working form anonymous class");
            }
        }).start();

        //Using lambdas. When there is no params () must be used.
        new Thread(() -> {
            System.out.println("working form lambda");
        }).start();

        //Simple lambda.
        new Thread(() -> System.out.println("working form simple lambda")).start();

    }

}
