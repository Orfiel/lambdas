package pl.codementors.lambdas.model;

/**
 * Simple namned text formatter.
 */
public class NamedFormatter implements Formatter {

    @Override
    public String format(String text) {
        return text + " formatted by named formatter";
    }

}
